using Underlings.Input;
using Underlings.Inventory;
using Underlings.Player;
using UniRx;
using UnityEngine;
namespace Underlings.FSM
{
    public class Starter : MonoBehaviour
    {
        [SerializeField] 
        private FSMFacade _facade;
        [SerializeField]
        private PlayerSystemFacade _playerSystemFacade;
        [SerializeField]
        private InventoryFacade inventoryFacade;
        [SerializeField]
        private int _initialState;
        [SerializeField]
        private int _finalState;
        [SerializeField]
        private LifeSystemFacade _lifeSystemFacade;
        [SerializeField]
        PantrySystemFacade _pantrySystemFacade;
        [SerializeField]
        CookbookSystemFacade _cookbookSystemFacade;
        [SerializeField]
        InputSystemFacade _InputSystemFacade;
        private void Awake()
        {
            _facade.Init(_initialState, _finalState).Concat(_facade.Load(_initialState)).Subscribe();
            _playerSystemFacade.Init();
            inventoryFacade.Init();
            _lifeSystemFacade.Init();
            _pantrySystemFacade.Init();
            _cookbookSystemFacade.Init();
            _InputSystemFacade.Init();
        }
    }
}
