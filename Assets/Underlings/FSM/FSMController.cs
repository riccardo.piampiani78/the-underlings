using System;
using System.Collections.Generic;
using System.Linq;
using Underlings.Enum;
using Underlings.Interfaces;
using UniRx;
using UnityEngine;
namespace Underlings.FSM
{
    public class FSMController : MonoBehaviour
    {
        [SerializeField]
        private List<GameObject> _systemGo = new List<GameObject>();
        private List<IUnderlingsSystem> _systems = new List<IUnderlingsSystem>();
        private Dictionary<EUSystemType, bool> _systemReady = new Dictionary<EUSystemType, bool>();
        private int _startingLevel;
        private int _endState;
        [SerializeField]
        private int _actualState;
        private CompositeDisposable _initialDisposables;
        private CompositeDisposable _loadLevelDisposables;
        
        //Only for debug:
        public List<string> SystemNotReady = new List<string>();
        //OnlyForDebug
        public IObservable<Unit> Init(int startingLevel, int endLevel)
        {
            return Observable.Create<Unit>(obs => {
                foreach (var go in _systemGo)
                {
                    _systems.Add(go.GetComponent<IUnderlingsSystem>());
                }
                _startingLevel = startingLevel;
                _endState = endLevel;
                _initialDisposables?.Dispose();
                _initialDisposables = new CompositeDisposable();
                IObservable<Unit> o = null;
                foreach (var s in _systems)
                {
                    _systemReady.Add(s.GetType(), false); //tutti i sistemi a not ready
                    s.Ready().Subscribe(HandleSystemReady).AddTo(_initialDisposables);
                    s.Reload().Subscribe(HandleSystemReload).AddTo(_initialDisposables);
                    o = o == null ? s.Init() : o.Merge(s.Init());
                }
               
                return o.Subscribe(obs.OnNext, obs.OnCompleted);
            });
      
        }

        private void HandleSystemReady(EUSystemType type)
        {
            _systemReady[type] = true;
            UpdateListDebug();
            if (_systemReady.Values.All(value => value))
            {
                SetAllSystemNotReady();
                _actualState++;
                Debug.Log($"--- FSM: STATE SETTED {_actualState} ---");
                if (_actualState >= _endState)
                {
                    Debug.Log($"Finito a livello {_actualState}");
                    return;
                }
                GoState();
            }

        }

        private void HandleSystemReload (EUSystemType type)
        {
            SetAllSystemNotReady();
            GoState();

        }

        public void SetAllSystemNotReady()
        {
            foreach (var key in _systemReady.Keys.ToList())
            {
                _systemReady[key] = false;
            }
        }

        private void GoState()
        {
            _loadLevelDisposables?.Dispose();
            _loadLevelDisposables = new CompositeDisposable();
            foreach (var s in _systems)
            {
                s.LoadState(_actualState).Subscribe().AddTo(_loadLevelDisposables);
            }
            UpdateListDebug();
        }

        public IObservable<Unit> Load(int state)
        {
            return Observable.Create<Unit>(obs =>
            {
                _actualState = state;
                GoState();
                obs.OnNext(Unit.Default);
                obs.OnCompleted();
                return Disposable.Empty;
            });
       
        }

        private void UpdateListDebug()
        {
            SystemNotReady.Clear();
            SystemNotReady.AddRange(_systems.Where(y => 
                    !_systemReady[y.GetType()])
                .Select(x => x.GetType()
                    .ToString()));
        }


    }
}






