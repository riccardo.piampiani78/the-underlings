using System;
using UniRx;
using UnityEngine;
namespace Underlings.FSM
{
    public class FSMFacade : MonoBehaviour
    {
        [SerializeField] private FSMController _controller;
        public IObservable<Unit> Init(int startingLevel, int endLevel)
        {
            return _controller.Init(startingLevel, endLevel);
        }

        public IObservable<Unit> Load(int state)
        {
            return _controller.Load(state);
        }
    }
}


