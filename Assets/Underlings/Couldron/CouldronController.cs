using System;
using System.Collections.Generic;
using System.Linq;
using Underlings.Inventory;
using Underlings.Items;
using UniRx;
using UnityEngine;

public class CouldronController : MonoBehaviour
{
    public CouldronView _view;
    private Subject<Unit> _onDishReady = new Subject<Unit>();
    public IObservable<Unit> OnDishReady() => _onDishReady.AsObservable();
    private List<Item> _itemInsideCouldron = new List<Item>();
    public List<Item> _recipeReady = new List<Item>();    
    public List<Item> GetRecipeReady => _recipeReady;    
    public IObservable<Item> OnPress()
    {
        return _view.OnPress();
    }
    
    public void Give(Item param)
    {
        _itemInsideCouldron.Add(param);
        Debug.Log($"dentro il calderone ci sono{_itemInsideCouldron.Count} ingredienti");
    }

    public bool TryGive(Item param)
    {


        return !param.IsTool;
    }

    public void Interact(Item param)
    { 
        if (param.id == "Mestolo")
        {
            if (_itemInsideCouldron.Count != 0)
            {
                _recipeReady = _itemInsideCouldron.ToList();                
                _itemInsideCouldron.Clear();
                _onDishReady.OnNext(Unit.Default);
            }
        }           
    }
}