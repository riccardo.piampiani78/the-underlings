﻿using System;
using Underlings.Interfaces;
using Underlings.Items;
using UniRx;
using UnityEngine;

public class CouldronView : MonoBehaviour
{
    private Subject<Item> _elementAdded = new Subject<Item>();
    public IObservable<Item> OnPress() => _elementAdded.AsObservable();

}