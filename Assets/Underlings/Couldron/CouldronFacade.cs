using System;
using System.Collections.Generic;
using Underlings.Enum;
using Underlings.Interfaces;
using Underlings.Inventory;
using Underlings.Items;
using Underlings.Player;
using UniRx;
using UnityEngine;
using UnityEngine.InputSystem;
namespace Underlings.Couldron
{
    public class CouldronFacade : MonoBehaviour, IUnderlingsGivable
    {

        [SerializeField]
        private CouldronController _controller;
        public IObservable<Item> OnPress()
        {
            return _controller.OnPress();
        }
        public IObservable<Unit> OnDishReady() => _controller.OnDishReady();
        public List<Item> GetRecipeReady => _controller.GetRecipeReady;
        public void Interact (Item param)
        {
            _controller.Interact(param);
        }
        public void Give(Item param)
        {
            _controller.Give(param);
            Debug.Log($"mi hai passato{param.Name}");
        }

        public bool TryGive(Item param)
        {
            Debug.Log(" c'e' spazio");
           return _controller.TryGive(param);
        }
    }
}