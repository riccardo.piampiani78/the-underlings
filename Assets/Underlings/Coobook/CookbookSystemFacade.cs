
using System;
using UniRx;
using UnityEngine;
public class CookbookSystemFacade : MonoBehaviour
{
    
    [SerializeField]
    private CookbookSystemController _cookbookSystemController;
    public IObservable<Unit> StateCookbook => _cookbookSystemController.StateCookbook;

    public void Init()
    {
        _cookbookSystemController.Init();
        
    }

    public void Close()
    {
        _cookbookSystemController.Close();
    }

    public void Open()
    {
        _cookbookSystemController.Open();
    }
}