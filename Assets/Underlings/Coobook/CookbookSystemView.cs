using System;
using System.Collections.Generic;
using Underlings.Inventory;
using UniRx;
using UnityEngine;
using Item = Underlings.Items.Item;

public class CookbookSystemView : MonoBehaviour
{
    internal void Close()
    {
        gameObject.SetActive(false);
    }

    internal void Open()
    {
        gameObject.SetActive(true);        
    }
}
