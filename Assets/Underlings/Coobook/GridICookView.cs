using System;
using TMPro;
using Underlings.Items;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
namespace Underlings.Inventory
{
    public class GridCookView : MonoBehaviour
    {
        [SerializeField]
        private Image _image;
        [SerializeField]
        private TextMeshProUGUI _name;
        [SerializeField]
        private TextMeshProUGUI _description;
        public void BindModel(Item item)
        {
            _image.sprite = item.Icon;
            _name.text = item.Name;
            _description.text = item.Description;

        }
    }
}