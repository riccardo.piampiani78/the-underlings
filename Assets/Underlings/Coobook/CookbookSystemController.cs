using System;
using System.Collections.Generic;
using System.Linq;
using Underlings.Inventory;
using UniRx;
using UnityEngine;
using Item = Underlings.Items.Item;
using UnityEditor;
using UnityEngine.UI;

public class CookbookSystemController : MonoBehaviour
{
    [SerializeField]
    private InventoryFacade _inventoryFacade;
    private Dictionary<string, Item> _models = new Dictionary<string, Item>();
    private Dictionary<string, GameObject> _items = new Dictionary<string, GameObject>();
    [SerializeField]
    private GameObject _gridPrefab;
    [SerializeField]
    private Transform _anchorTrasformCook;
    [SerializeField]
    private CookbookSystemView _cookbookSystemView;
    private Subject<Unit> _stateCookbook = new Subject<Unit>();
    public IObservable<Unit> StateCookbook => _stateCookbook.AsObservable();
    private int _currentPage = 0;
    [SerializeField]
    private Button _nextPage;
    [SerializeField]
    private Button _previousPage;


    internal void AddItem(Item param)
    {
        Debug.Log("ho preso l oggetto "+ param.name+"nel libro");
        {
            if (_models.ContainsKey(param.id))
            {
                return;
            }
            _models.Add(param.id, param);
          
        }
    }

    private void CheckOpening(Item param)
    {
        if (param.id == "Cookbook")
        {
            _stateCookbook.OnNext(Unit.Default);
        }
    }

    public void Init()
    {
        _inventoryFacade.OnItemCollected.Subscribe(AddItem);
        _inventoryFacade.OnItemSelected.Subscribe(CheckOpening);
        _nextPage.onClick.AddListener(NextPage);
        _previousPage.onClick.AddListener(PreviousPage);
    }

    public void Open()
    {
        _cookbookSystemView.Open();
        RefreshPage();
    }

    internal void Close()
    {
        _cookbookSystemView.Close();
    }

    private void NextPage()
    {
        if (_currentPage < (_models.Count - 1) / 4)
        {
            _currentPage++;
            RefreshPage();
        }
        else
        {
            Debug.Log("non ho girato pagina");
        }

    }

    private void PreviousPage()
    {
        if (_currentPage != 0)
        {

            _currentPage--;
            RefreshPage();

        }
        else
        {
            Debug.Log("non ho girato pagina");
        }
    }

    private void RefreshPage()
    {
        foreach (var item in _items)
        {
            Destroy(item.Value);
        }
        _items.Clear();
        var numberOfElement = _models.Count - _currentPage * 4;
        if (numberOfElement > 4)
        {
            numberOfElement = 4;
        }
        var m = _models.Select(x => x.Value).ToList().GetRange(_currentPage * 4, numberOfElement);
        foreach (var item in m)
        {           
            var p = Instantiate(_gridPrefab);
            var f = p.GetComponent<GridCookFacade>();
            p.transform.parent = _anchorTrasformCook;
            f.BindModel(item);
            _items.Add(item.id, p);
        }
    }
}