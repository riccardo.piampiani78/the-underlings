using System;
using Underlings.Items;
using UnityEngine;
namespace Underlings.Inventory
{
    public class GridCookController : MonoBehaviour
    {
        [SerializeField]
        private GridCookView _view;

        
        public void BindModel(Item item)
        {
            _view.BindModel(item);
        }
    }
}