using System;
using Underlings.Items;
using UnityEngine;
namespace Underlings.Inventory
{
    public class GridCookFacade : MonoBehaviour
    {
        [SerializeField]
        private GridCookController _controller;
       
        public void BindModel (Item item)
        {
            _controller.BindModel(item);
        }
    }
}