using System;
using Underlings.Interfaces;
using UnityEngine;
namespace Underlings.Items.ItemWorld
{
    public class ItemWorldFacade : MonoBehaviour, IUnderlingsInteractable, IUnderlingsCollectable
    {

        [SerializeField] 
        private ItemWorldController _itemWorldController;
    
        public IObservable<Item> OnItemCollected()
        {
            return _itemWorldController.OnItemCollected();
        }
        public void BindModel(Item model)
        {
            _itemWorldController.BindModel(model);
        }

        public Item GetModel()
        {
            return _itemWorldController.GetModel();
        }
        public Item GetModelAndDestroy()
        {
            return _itemWorldController.GetModelAndDestroy();
        }

        public void Interact()
        {
            _itemWorldController.Interact();
        }
    }
}