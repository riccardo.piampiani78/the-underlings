using System;
using UniRx;
using UnityEngine;
namespace Underlings.Items.ItemWorld
{
    public class ItemWorldController : MonoBehaviour
    {
        private Item _model;
        private Subject<Item> _onInteract = new Subject<Item>();
        [SerializeField] 
        private ItemWorldView _itemWorldView;
            
        public void BindModel(Item model)
        {
            _model = model;
            _itemWorldView.BindModel(model);
        }

        public Item GetModel()
        {
            return _model; //questo metodo ritorna il modello dell�oggetto
        }

        public Item GetModelAndDestroy()
        {
            // Ottieni il modello prima di distruggere l'oggetto
            Item model = GetModel();
            gameObject.SetActive(false);
            DestroyImmediate(this);
            // Ritorna il modello ottenuto
            return model;
        }

        public IObservable<Item> OnItemCollected() => _onInteract.AsObservable();

        public void Interact()
        {
             gameObject.SetActive(false);
            _onInteract.OnNext(_model);
            _onInteract.OnCompleted();
        }
    }
}