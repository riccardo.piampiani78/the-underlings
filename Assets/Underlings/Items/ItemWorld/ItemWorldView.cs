using UnityEngine;
namespace Underlings.Items.ItemWorld
{
    public class ItemWorldView : MonoBehaviour
    {
        [SerializeField]
        private SpriteRenderer _spriteRenderer;
        public void BindModel(Item model)
        {
            _spriteRenderer.sprite = model.Image;
        }
    }
}