using System;
using Underlings.Enum;
using Underlings.Interfaces;
using Underlings.Items.ItemWorld;
using UniRx;
using UnityEngine;
namespace Underlings.Items.ItemSystem
{
    public class ItemsSystemFacade : MonoBehaviour, IUnderlingsSystem
    {
        [SerializeField]
        private ItemsSystemController _itemsSystemController;

        public IObservable<Unit> Init()
        {
            //qua ci va messa la chiamata all'item directory
            return _itemsSystemController.Init();
        }

        public IObservable<Unit> LoadState(int state)
        {
            return _itemsSystemController.LoadState(state);
        }

        public EUSystemType GetType()
        {
            return _itemsSystemController.GetType();
        }

        public IObservable<EUSystemType> Ready()
        {
            return _itemsSystemController.Ready();
        }

        public IObservable<EUSystemType> Reload()
        {
            return _itemsSystemController.Reload();
        }

        public Item GetItemModel(string id)
        {
            return _itemsSystemController.GetItemModel(id);    // Questo metodo ritorna direttamente il modello Item caricato dal sistema ItemDirector}
        }

        public ItemWorldFacade GetItemWorld(string id)
        {
            return _itemsSystemController.GetItemWorld(id);
        }
    

    }
}