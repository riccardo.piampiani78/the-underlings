using System;
using Underlings.Enum;
using Underlings.Items.ItemWorld;
using UniRx;
using UnityEngine;
namespace Underlings.Items.ItemSystem
{
    public class ItemsSystemController : MonoBehaviour
    {
        [SerializeField]
        private ItemDirectory _itemDirectory;
        [SerializeField]
        private ItemWorldFacade _worldItemPrefab;

        private Subject<EUSystemType> _ready = new Subject<EUSystemType>();
    #region InterfaceMethods
        public IObservable<Unit> Init()
        {         
            return Observable.Empty<Unit>();
        }

        public IObservable<Unit> LoadState(int state)
        {
            return Observable.Create<Unit>(obs =>
            {
                Debug.Log($"InitSystemController {state}");        
                _ready.OnNext(EUSystemType.ItemSystem);
                return Disposable.Empty;
            });
        }
    

        public EUSystemType GetType()
        {
            return EUSystemType.ItemSystem;
        }

        public IObservable<EUSystemType> Ready()
        {
            return _ready.AsObservable();
        }

        public IObservable<EUSystemType> Reload()
        {
            return Observable.Empty<EUSystemType>();
        }
    #endregion

        public  Item GetItemModel (string id)
        {
            return _itemDirectory.GetItemById(id);
        }

        public ItemWorldFacade GetItemWorld(string id)
        {
            var p = Instantiate(_worldItemPrefab);
        
            p.BindModel(_itemDirectory.GetItemById(id));
            return p;
        }
    }
}