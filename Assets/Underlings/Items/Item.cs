using Sirenix.OdinInspector;
using Underlings.Enum;
using UnityEngine;
namespace Underlings.Items
{
    [CreateAssetMenu(fileName = "NewItem", menuName = "Underlings/CreateItem")]
    public class Item : ScriptableObject
    {
        public string id;
        public string Name;
        public string Description;
        public Sprite Image;
        public Sprite Icon;
        public bool IsTool;
        public bool ShowMessage;
        [ShowIf("ShowMessageFunction")]
        public string MessageOnPick;
        [ShowIf("ShowMessageFunction")]
        public bool isPopUp;
        private bool ShowMessageFunction() { return ShowMessage == true; }

    }
}