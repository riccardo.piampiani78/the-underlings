using System.Collections.Generic;
using UnityEngine;
namespace Underlings.Items
{
    public class ItemDirectory : MonoBehaviour
    {
        [SerializeField]
        private List<Item> itemsList = new List<Item>();
    

        public Item GetItemById(string id)
        {
            return itemsList.Find(item => item.id == id);
        }
    }
}