using System;
using Underlings.Enum;
using UniRx;
namespace Underlings.Interfaces
{
    public interface IUnderlingsInteractable
    {
        public void Interact();
    }
}