using System;
using Underlings.Enum;
using UniRx;
namespace Underlings.Interfaces
{
    public interface IUnderlingsSystem
    {
        EUSystemType GetType();
        IObservable<Unit> Init();
        IObservable<Unit> LoadState(int state);
        IObservable<EUSystemType> Ready();
        IObservable<EUSystemType> Reload();

    }
}