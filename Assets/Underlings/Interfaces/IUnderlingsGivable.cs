using Underlings.Items;
namespace Underlings.Interfaces {
    public interface IUnderlingsGivable
    {
        bool TryGive(Item param);
        void Give(Item param);
        void Interact (Item param);
    }
}