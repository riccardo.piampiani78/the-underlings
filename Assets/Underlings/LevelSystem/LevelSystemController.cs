using System;
using System.Collections.Generic;
using System.Linq;
using Underlings.Enum;
using Underlings.Items;
using Underlings.Items.ItemSystem;
using Underlings.Items.ItemWorld;
using Underlings.Rooms;
using Underlings.Rooms.Doors;
using UniRx;
using UnityEngine;
namespace Underlings.LevelSystem
{
    public class LevelSystemController : MonoBehaviour
    {
        [SerializeField] private List<RoomSystemFacade> Rooms = new List<RoomSystemFacade>();
        [SerializeField] private List<DoorSystemFacade> Doors = new List<DoorSystemFacade>();
        [SerializeField] private LevelDataStructure _stateRoomsList;
        [SerializeField] private DoorsDataStructure _stateDoorList;
        [SerializeField] private ItemsSystemFacade _itemsSystemFacade;
        private Subject<string> _onPlayerEnterInRoom = new Subject<string>();
        public IObservable<string> OnPlayerEnterInRoom() => _onPlayerEnterInRoom.AsObservable();
        private Subject<EUSystemType> _onReady = new Subject<EUSystemType>();
        private CompositeDisposable _disposables = new CompositeDisposable();
        private int _actualState;
        public string PlayerPositionIdRoom;
        public IObservable<Unit> Init()
        {
            return Observable.Create<Unit>(obs =>
            {
                _disposables?.Dispose();
                _disposables = new CompositeDisposable();
                var ListObs = new List<IObservable<Unit>>();
                foreach (RoomSystemFacade r in Rooms)
                {
                    r.OnItemCollected().Subscribe(OnItemCollected).AddTo(_disposables);
                    r.OnPlayerEnterInRoom().Subscribe(OnPlayerEnterInRoom).AddTo(_disposables);
                    ListObs.Add(r.OnReady());
                    r.Init();
                }
                var CombinedObs = ListObs.Zip();
                CombinedObs.Subscribe(x =>
                {
                    _onReady.OnNext(EUSystemType.Level);
                });
                obs.OnNext(Unit.Default);
                obs.OnCompleted();

                return Disposable.Empty;
            });
        }

        private void OnItemCollected(Tuple<string, Item> param)  
        {
            foreach (StateToRoomsLevel level in _stateRoomsList.stateToRoomsList)
            {
                if (level.FSMState == _actualState)
                {
                    var room = level.Rooms.Where(x => x.IdRoom == param.Item1).FirstOrDefault();
                    foreach (ItemState i in room.Items)
                    {
                        if (i.IdItem == param.Item2.id)
                        {
                            //      i.IsOn = false;     questa riga e' per il salvataggio dello stato degli oggetti
                        }
                    }
                }

            }

            //AssetDatabase.SaveAssets(); da abilitare quando vogliamo salvare il file

        }

        private void OnPlayerEnterInRoom(string idRoom)
        {
            PlayerPositionIdRoom = idRoom;
            _onPlayerEnterInRoom.OnNext(idRoom);
        }


        public IObservable<Unit> LoadState(int state)
        {
            return Observable.Create<Unit>(obs =>
            {
                _actualState = state;
                BindtoRoomState();
                BindtoDoorState();
                obs.OnNext(Unit.Default);
                obs.OnCompleted();
                return Disposable.Empty;
            });
        }

        public IObservable<EUSystemType> Ready()
        {
            return _onReady.AsObservable();
        }

        public IObservable<EUSystemType> Reload()
        {
            return Observable.Empty<EUSystemType>();
        }

        public EUSystemType GetType()
        {
            return EUSystemType.Level;
        }

        private void BindtoRoomState(){
            bool StateFound = false;
            foreach (var level in _stateRoomsList.stateToRoomsList)
            {
                if (level.FSMState == _actualState)
                {
                    StateFound = true;
                    foreach (var roomFacade in Rooms.Where(roomFacade => !level.Rooms.Select(x => x.IdRoom).Contains(roomFacade.GetId())))
                    {
                        roomFacade.BindItem(new Dictionary<string, ItemWorldFacade>(), false);
                    }

                    foreach (var room in level.Rooms)
                    {
                        
                        var roomFacade = Rooms.FirstOrDefault(x => x.GetId() == room.IdRoom);
                        Dictionary<string,ItemWorldFacade> ItemToPass = new Dictionary<string,ItemWorldFacade>();
                        foreach(var item in room.Items)
                        {
                            if (item.IsOn == true)
                                ItemToPass.Add(item.IdRoomSpawnPoint, _itemsSystemFacade.GetItemWorld(item.IdItem));
                        }

                        roomFacade.BindItem(ItemToPass, room.RequireEnter);

                    }
                }

                    
            }
            if (StateFound == false)
            {
                _onReady.OnNext(EUSystemType.Level);
            }
        }
        private void BindtoDoorState()
        {
            foreach (var level in _stateDoorList.stateToRoomsList)
            {
                if (level.FSMState == _actualState)
                {
                    foreach (var room in level.Rooms)
                    {
                        List<DoorSystemFacade> doorFacade = Doors.Where(x => x.GetRoomId() == room.IdRoom).ToList();
                    
                        foreach (var door in room.Doors)
                        {
                            var _selectedDoor = doorFacade.Where(x => x.GetDoorId() == door.idDoor).FirstOrDefault();
                            _selectedDoor.SetState(door.State);
                        }

                    }
                }

            }
        }

        public Transform GetSpawnPointTransform(string id, string spawnId)
        {
            var room = Rooms.Where(x => x.GetId() == id).FirstOrDefault();
            return room.GetSpawnPointView(spawnId).transform;
        }
    }
}