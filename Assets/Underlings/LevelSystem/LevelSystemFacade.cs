using System;
using Underlings.Enum;
using Underlings.Interfaces;
using UniRx;
using UnityEngine;
namespace Underlings.LevelSystem
{
    public class LevelSystemFacade : MonoBehaviour, IUnderlingsSystem
    {
        [SerializeField]
        private LevelSystemController _controller;
        public IObservable<string> OnPlayerEnterInRoom() => _controller.OnPlayerEnterInRoom();
        public IObservable<Unit> Init()
        {
            return _controller.Init();
        }

        public string GetPlayerPositionRoomId()
        {
            return _controller.PlayerPositionIdRoom;
        }
        public IObservable<Unit> LoadState(int state)
        {
            return _controller.LoadState(state);
        }

        public IObservable<EUSystemType> Ready()
        {
            return _controller.Ready();
        }

        public IObservable<EUSystemType> Reload()
        {
            return _controller.Reload();
        }

        public EUSystemType GetType()
        {
            return _controller.GetType();
        }

        public Transform GetSpawnPointTransform(string idRoom, string spawnId) {

            return _controller.GetSpawnPointTransform(idRoom, spawnId);



        }
    }
}

