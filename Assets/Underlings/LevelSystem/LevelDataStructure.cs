using System;
using System.Collections.Generic;
using UnityEngine;
namespace Underlings.LevelSystem
{
    [CreateAssetMenu(fileName = "NewLevelData", menuName = "Underlings/Level/LevelData")]
    public class LevelDataStructure : ScriptableObject
    {
        public List<StateToRoomsLevel> stateToRoomsList = new List<StateToRoomsLevel>();

    }

    [Serializable]
    public class ItemState
    {
        public string IdItem;
        public string IdRoomSpawnPoint;
        public bool IsOn;
    }

    [Serializable]
    public class RoomDataLevel
    {
        public string IdRoom;
        public List<ItemState> Items;
        public bool RequireEnter;
    }

    [Serializable]
    public class StateToRoomsLevel
    {
        public int FSMState;
        public List<RoomDataLevel> Rooms;
    }
}