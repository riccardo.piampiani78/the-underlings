using System;
using System.Collections.Generic;
using Underlings.Enum;
using UnityEngine;
namespace Underlings.LevelSystem
{
    [Serializable]
    public class Door
    {
        public int idDoor;
        public int idConnectedDoor;
        public EUDoorState State;
    }

    [Serializable]
    public class RoomDoorsData
    {
        public string IdRoom;
        public List<Door> Doors;
    }

    [Serializable]
    public class RoomToRoom
    {
        public int FSMState;
        public List<RoomDoorsData> Rooms;
    }


    [CreateAssetMenu(fileName = "NewRoomsData", menuName = "Underlings/Level/DoorsData")]
    public class DoorsDataStructure : ScriptableObject
    {
        public List<RoomToRoom> stateToRoomsList;
    }
}