using UnityEngine;
namespace Underlings.Player
{
    public class PlayerSystemFacade : MonoBehaviour
    {
        [SerializeField]
        private PlayerSystemController _playerSystemController;

        public void Init()
        {
            _playerSystemController.Init();
        }
        public void Move(Vector2 param)
        {
            _playerSystemController.Move(param);
        }

        public void Interact()
        {
            _playerSystemController.Interact();
        }
    }
}