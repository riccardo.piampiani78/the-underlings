using System;
using UnityEngine;
namespace Underlings.Player
{
    public class PlayerSystemView : MonoBehaviour
    {
        [SerializeField]
        private Rigidbody2D rigidbody;
        [SerializeField]
        private Animator animator;
        private GameObject _currentCollision;
        [SerializeField]
        private SpriteRenderer _spriteRenderer;
        private Vector2 _lastVelocity = new Vector2();
        public void SetVelocity(Vector2 velocity)
        {
            rigidbody.velocity = velocity;
            animator.SetFloat("Horizontal", Math.Abs(velocity.x));
            _spriteRenderer.flipX = velocity.x > 0;
            animator.SetFloat("Vertical", velocity.y);
            _lastVelocity = velocity;
        }

       public void OnTriggerEnter2D(Collider2D collision)
        {
            _currentCollision = collision.gameObject; //al momento questo tipo di collisione puo' gestire solo UNA collisione
            //con un oggetto. nel caso ci sia bisogno creeremo una lista di gameobject
        }                                             //con cui ho colliso

        public void OnTriggerExit2D(Collider2D collision)
        {
            _currentCollision = null;
        }

        public void OnCollisionEnter2D(Collision2D collision) 
        {
            _currentCollision = collision.gameObject;
        }

        public void OnCollisionExit2D(Collision2D collision)
        {
            _currentCollision = null;
            SetVelocity(_lastVelocity);
        }

        public GameObject GetCurrentCollision()
        {
        
            return _currentCollision;
        
        }
    }
}