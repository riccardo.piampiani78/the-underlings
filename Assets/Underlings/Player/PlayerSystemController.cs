using System;
using Underlings.Interfaces;
using Underlings.Inventory;
using Underlings.Items;
using Underlings.Items.ItemWorld;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
namespace Underlings.Player
{
    public class PlayerSystemController : MonoBehaviour
    {
        [SerializeField]
        private PlayerSystemView _playerSystemView;
        private Vector2 _movementDir = new Vector2(0, 0);
        [SerializeField]
        private float Speed = 1;
        [SerializeField]
        private InventoryFacade _inventoryFacade;
        private IDisposable _onItemSelectedDisp;
        public void Init()
        {
            _onItemSelectedDisp?.Dispose();
            _onItemSelectedDisp = _inventoryFacade.OnItemSelected.Subscribe(ItemSelected);
        }

        public void ItemSelected(Item param)
        {
            var collision = _playerSystemView.GetCurrentCollision();
            if (collision != null)
            {
                var GOGivable = collision.GetComponent<IUnderlingsGivable>();
                if (GOGivable != null)
                {
                    if(GOGivable.TryGive(param))
                    {
                        GOGivable.Give(param);  
                        _inventoryFacade.RemoveItem(param);
                    }
                    else
                    {
                        GOGivable.Interact(param);
                    }
                        
                }
                
            }
        }
        public void Interact() { 
   
            var collision = _playerSystemView.GetCurrentCollision();
            if (collision != null)
            {
                var GOinteract = collision.GetComponent<IUnderlingsInteractable>();
                if (GOinteract != null)
                {
                    if (collision.GetComponent <IUnderlingsCollectable>() != null)
                    {
                        var model = collision.GetComponent<ItemWorldFacade>().GetModel();
                        if (_inventoryFacade.CanCollect(model))
                        {
                            GOinteract.Interact();
                            _inventoryFacade.Collect(model);
                        }
                    }
                    else
                    {
                        GOinteract.Interact();
                    }
                }
            }
        
        }

        public void Move(Vector2 param)
        {
            _movementDir = param;
            _playerSystemView.SetVelocity(_movementDir * Speed);
        }
    }
}