using UnityEngine.SceneManagement;
using UnityEngine;

public class LoadScene : MonoBehaviour
{ 
    public void ChangeScene()
    {                  
            SceneManager.LoadScene("MainScene");       
    }
}