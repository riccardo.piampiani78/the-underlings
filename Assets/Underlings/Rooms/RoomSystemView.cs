using System;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
namespace Underlings.Rooms
{
    public class RoomSystemView : MonoBehaviour
    {
        private Subject<Unit> _onPlayerEntered = new Subject<Unit>();
        public IObservable<Unit> OnPlayerEntered() => _onPlayerEntered.AsObservable();
        [SerializeField]
        private BoxCollider2D _boxCollider2D;
        public void Init ()
        {
            _boxCollider2D.OnTriggerEnter2DAsObservable().Subscribe(Collided);
        }
        private void Collided (Collider2D collider2D)
        {
            if (collider2D.gameObject.name == "Player")
            {
                _onPlayerEntered.OnNext(Unit.Default);
            }
        }
    }
}