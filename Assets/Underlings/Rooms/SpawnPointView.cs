using UnityEngine;
namespace Underlings.Rooms
{
    public class SpawnPointView : MonoBehaviour
    {

        [SerializeField] private string _id;
        public string GetId() => _id;
    }
}
