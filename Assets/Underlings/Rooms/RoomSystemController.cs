using System;
using System.Collections.Generic;
using System.Linq;
using Underlings.Enum;
using Underlings.Items;
using Underlings.Items.ItemWorld;
using Underlings.Rooms.Doors;
using UniRx;
using UnityEngine;
using UnityEngine.InputSystem;
namespace Underlings.Rooms
{
    public class RoomSystemController : MonoBehaviour
    {
        private Subject <Tuple<string,Item>> _onItemCollected = new Subject<Tuple<string,Item>> ();
        private CompositeDisposable _collectedDisp = new CompositeDisposable ();
        public IObservable<Tuple<string, Item>> OnItemCollected() => _onItemCollected.AsObservable();
        [SerializeField]
        private RoomSystemView _roomSystemView;
        [SerializeField]
        private List<DoorSystemFacade> _allDoor = new List<DoorSystemFacade>();

        private Subject<string> _onPlayerEnterInRoom = new Subject<string>();
        public IObservable<string> OnPlayerEnterInRoom() => _onPlayerEnterInRoom.AsObservable();
        
        private Subject<Unit> _onReady = new Subject<Unit>();
        public IObservable<Unit> OnReady() => _onReady.AsObservable();

        private List<SpawnPointView> _spawnPoint = new List<SpawnPointView>();
        [SerializeField] private string _roomId;
        [SerializeField] private string _roomName;
        private Dictionary<string, Dictionary<string,ItemWorldFacade>> _itemWorldFacade = new Dictionary<string, Dictionary<string,ItemWorldFacade>>();
        private bool _requireEnter = false;
        public void Init()
        {
            _spawnPoint = transform.GetComponentsInChildren<SpawnPointView>().ToList();
            _roomSystemView.Init();
            _roomSystemView.OnPlayerEntered().Subscribe(HandleOnPlayerEntered);
        }
        public string GetRoomId() => _roomId;
        public IObservable<Unit> Show() { 

            return Observable.Empty<Unit>();

        } //Questi metodi sono osservabili perch� potremmo fare in modo che si accendano e spengano in maniera animata
        public IObservable<Unit> Hide() {

            return Observable.Empty<Unit>();
        }

        public void SetDoorState(Dictionary<int, EUDoorState> doorState)      //questo metodo serve per settare lo stato
        {                                                                   // di determinate porte scelte con l'id
            foreach (var door in _allDoor) { 
        
                var id = door.GetDoorId();
                if (doorState.ContainsKey(id)) {

                    door.SetState(doorState[id]);

                }

            }
        }

        public void ToggleItems(Dictionary<string, bool> itemToggle)
        {
            /*foreach(var item in itemToggle)
            {
                if (_itemWorldFacade.ContainsKey(item.Key))
                {
                    _itemWorldFacade[item.Key].gameObject.SetActive(item.Value);
                }
            }*/
        }

        public SpawnPointView GetSpawnPointView(string id)
        {
            return _spawnPoint.Where(s => s.GetId() == id).FirstOrDefault();
        }
        public void BindItem(Dictionary<string, ItemWorldFacade> itemToAdd, bool RequireEnter)
        {
            _requireEnter = RequireEnter;
            _collectedDisp?.Dispose();
            _collectedDisp = new CompositeDisposable();
            foreach (var item in itemToAdd)
            {
                var p =  GetSpawnPointView(item.Key).transform;
                item.Value.transform.SetParent(p);
                item.Value.transform.localPosition = new Vector3(0,0,0);
                item.Value.OnItemCollected().Subscribe(OnItemCollectedSub).AddTo(_collectedDisp);
                if (!_itemWorldFacade.ContainsKey(item.Key))
                {
                    _itemWorldFacade.Add(item.Key,new Dictionary<string, ItemWorldFacade>());
                }
                _itemWorldFacade[item.Key].Add(item.Value.GetModel().id,item.Value);
            }   
            if (_requireEnter == false)
            {
                _onReady.OnNext(Unit.Default);
            }

        }

        public void OnItemCollectedSub(Item param)
        {
            _onItemCollected.OnNext(new Tuple<string,Item>(_roomId,param));
        }

        private void HandleOnPlayerEntered(Unit u)
        {
            _onPlayerEnterInRoom.OnNext(_roomId);
            if(_requireEnter)
            {
                _requireEnter = false;
                _onReady.OnNext(Unit.Default);
            }
            
        }
       
    }
}


