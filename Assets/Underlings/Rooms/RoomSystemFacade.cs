using System;
using System.Collections.Generic;
using Underlings.Enum;
using Underlings.Items;
using Underlings.Items.ItemWorld;
using UniRx;
using UnityEngine;
namespace Underlings.Rooms
{
    public class RoomSystemFacade : MonoBehaviour 
    {
        public IObservable<string> OnPlayerEnterInRoom() => _roomSystemController.OnPlayerEnterInRoom();
        public IObservable<Tuple<string, Item>> OnItemCollected() => _roomSystemController.OnItemCollected();
        [SerializeField]
        private RoomSystemController _roomSystemController;

        public IObservable<Unit> OnReady() => _roomSystemController.OnReady();

        public void Init()
        {
            _roomSystemController.Init();
        }
        public void ToggleItems(Dictionary<string, bool> ItemToggle )
        {
            _roomSystemController.ToggleItems(ItemToggle);
        }
        public void SetDoorState(Dictionary<int, EUDoorState> setState)
        {
            _roomSystemController.SetDoorState(setState);
        }

        public string GetId()
        {
            return _roomSystemController.GetRoomId();
        }
        /* public bool TryOpenDoor(int doorId)
     {


     }*/
        public void BindItem(Dictionary<string, ItemWorldFacade> itemToAdd, bool RequireEnter)
        {
            _roomSystemController.BindItem(itemToAdd, RequireEnter);
            
        }

        public SpawnPointView GetSpawnPointView(string id)
        {
            return _roomSystemController.GetSpawnPointView(id);
        }
    }

    

}