using Underlings.Enum;
using UnityEngine;
namespace Underlings.Rooms.Doors
{
    public class DoorSystemController : MonoBehaviour
    {
        private EUDoorState _doorState = EUDoorState.Closed;
        [SerializeField] private int _id;
        [SerializeField] private string _idRoom;
        [SerializeField] private DoorSystemView _doorSystemView;
        public int GetDoorId() {

            return _id;
    
        }
        public void SetState(EUDoorState state) {

            _doorState = state;
            _doorSystemView.SetState(state);

        }
        public bool OpenDoor()
        {

            if (_doorState == EUDoorState.Opened)
            {
                SetState(EUDoorState.Closed);
                return true;
            }

            if (_doorState == EUDoorState.Closed)
            {
                SetState(EUDoorState.Opened);
                return true;
            }
            return false;
        }
        public bool CloseDoor() {

            return false;
    
        }

        public string GetRoomId() {
            return _idRoom;

        }
    }
}