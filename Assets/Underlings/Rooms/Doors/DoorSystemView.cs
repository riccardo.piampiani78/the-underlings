using Underlings.Enum;
using UnityEngine;
namespace Underlings.Rooms.Doors
{
    public class DoorSystemView : MonoBehaviour
    {
        [SerializeField]
        private SpriteRenderer _spriteRenderer;
        [SerializeField]
        private Sprite _closedDoor;
        [SerializeField]
        private Sprite _openedDoor;
        [SerializeField]
        private Sprite _lockedDoor;
        [SerializeField]
        private BoxCollider2D _boxCollider;
        public void SetState(EUDoorState state) {

            if (state == EUDoorState.Closed)
            {
                _spriteRenderer.sprite = _closedDoor;
                _boxCollider.isTrigger = false;
            }

            if (state == EUDoorState.Locked)
                _spriteRenderer.sprite = _lockedDoor;

            if (state == EUDoorState.Opened)
            {
                _spriteRenderer.sprite = _openedDoor;
                _boxCollider.isTrigger = true;
            }

        }
    
    }
}