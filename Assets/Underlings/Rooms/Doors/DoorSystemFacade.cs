using Underlings.Enum;
using Underlings.Interfaces;
using UnityEngine;
namespace Underlings.Rooms.Doors
{
    public class DoorSystemFacade : MonoBehaviour, IUnderlingsInteractable
    {
        [SerializeField]
        private DoorSystemController _doorSystemController;
        public int GetDoorId() { 

            return _doorSystemController.GetDoorId();

        }

        public string GetRoomId()
        {
            return _doorSystemController.GetRoomId();
        }
        public void SetState(EUDoorState state) {

            _doorSystemController.SetState(state);
        }
        public bool OpenDoor() { 
    
            return _doorSystemController.OpenDoor();    
        }
        public bool CloseDoor() {

            return _doorSystemController.CloseDoor();
    
        }

        public void Interact()
        {
            OpenDoor();
        }
    }
}
