using System;
using System.Collections.Generic;
using Underlings.Enum;
using Underlings.Items;
using Underlings.Recipe;
using UniRx;
using UnityEngine;
using System.Linq;

public class RecipeSystemFacade : MonoBehaviour
{
    [SerializeField]
    RecipeSystemController _recipeSystemController;    
    
    public Recipe GetRecipeById(string id)
    {
        return _recipeSystemController.GetRecipeById(id);       

    }
    public ERecipeValidation ValidateRecipe(string idRecipe, List<Item> item)
    {
        return _recipeSystemController.ValidateRecipe(idRecipe, item);
    }  
}

