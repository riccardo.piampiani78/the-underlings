using System.Collections.Generic;
using UnityEngine;
namespace Underlings.Recipe
{
    [CreateAssetMenu(fileName = "NewRecipe", menuName = "Underlings/CreateRecipe")]
    public class Recipe : ScriptableObject
    {
        public string Id;
        public string Name;
        public string Description;
        public int MaxItemsNumber;
        public List<string> EssentialItems;
        public List<string> OptionalItems;
    }
}