using System.Collections.Generic;
using System.Linq;
using Underlings.Enum;
using Underlings.Recipe;
using UnityEngine;
using Underlings.Items;

public class RecipeSystemController : MonoBehaviour
{
    public List<Recipe> Recipes;
    private bool _essentialItems;
    private bool _optionalItems;
    public Recipe GetRecipeById(string id)
    {
        
        foreach (Recipe recipe in Recipes)
        {
            if (recipe.Id == id)
            {
                return recipe;
            }
        }
        return null;

    }

    public ERecipeValidation ValidateRecipe(string idRecipe, List<Item> item)
    {       
        //dobbiamo valutare se c'e' anche un numero massimo di elementi che uno puo' buttare nel calderone.
        //per esempio, a meta' gioco uno puo' dire "ok, ce ne butto 10 dentro, vedrai almeno gli Essenziali li becco"
        _essentialItems = false;
        _optionalItems = false;
        var recipe = GetRecipeById(idRecipe);
        List<string> dishItems = new List<string>();
        foreach (Item item2 in item)
        {
            dishItems.Add(item2.id);
        }

        if (recipe.EssentialItems.All(elem => dishItems.Contains(elem)))
        {
            _essentialItems = true;
        }

        if (recipe.OptionalItems.All(elem => dishItems.Contains(elem)))
        {
            _optionalItems = true;
        }

        if (_essentialItems && _optionalItems && recipe.MaxItemsNumber>=dishItems.Count)
        {
            return ERecipeValidation.Perfect;
        }

        if (_essentialItems)
        {
            return ERecipeValidation.OnlyEssential;
        }

        else
        {
            return ERecipeValidation.Failed;
        }
    }
}