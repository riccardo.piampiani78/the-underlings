using UnityEngine;

public class Camera : MonoBehaviour
{
    [SerializeField]
    private Transform _playerTransform;
    public void Update()
    {
        Vector3 vector3 = _playerTransform.position;
        vector3.z = -1;
        transform.position = vector3;
    }
}