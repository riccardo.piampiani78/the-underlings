using UnityEngine;
using UnityEngine.InputSystem;
namespace Underlings.Input
{
    public class InputSystemFacade : MonoBehaviour

    {
        [SerializeField] private InputSystemController _controller;

        public void Init()
        {
            _controller.Init();

        }


        public void MovingAction(InputAction.CallbackContext delta)
        {
            _controller.MovingAction(delta);

        }

        public void InteractButton(InputAction.CallbackContext delta)
        {
            _controller.InteractButton(delta);
        }

        public void InventoryButton(InputAction.CallbackContext delta)
        {
            _controller.InventoryButton(delta);
        }

        public void ToggleInput(bool param)
        {
            _controller.ToggleInput(param);
        }

    }
}
