using System;
using Underlings.Enum;
using Underlings.Inventory;
using Underlings.Player;
using Underlings.UISystem;
using UniRx;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Underlings.Input
{
    public class InputSystemController : MonoBehaviour
    {
        public EInputState _state = EInputState.Player;
        private EInputState _laststate = EInputState.Player;
        [SerializeField]
        private PlayerSystemFacade _playerSystemFacade;
        [SerializeField]
        private PantrySystemFacade _pantrySystemFacade;
        [SerializeField]
        private InventoryFacade _inventoryFacade;
        [SerializeField]
        private CookbookSystemFacade _cookbookSystemFacade;
        [SerializeField]
        private UIFacade _UIFacade;
        [SerializeField]
        private CountdownTimerFacade CountdownTimer;
        



        public void MovingAction(InputAction.CallbackContext param)
        {
            if (_state == EInputState.Wait)
            {
                return;
            }
            if (_UIFacade.AreThereOpenPanels())
            {
                return;
            }
            var delta = param.ReadValue<Vector2>();
            if (_state == EInputState.Player)
                _playerSystemFacade.Move(delta);        

        }

        public void InteractButton(InputAction.CallbackContext param) 
        {
            if (_state == EInputState.Wait)
            {
                return;
            }

            if (param.started != true)
            {
                return;
            }
            
            if (_UIFacade.GoToNextPanel())
            {
                return;
            }

            if (_state == EInputState.Player)
                _playerSystemFacade.Interact();
        }         

        public void InventoryButton(InputAction.CallbackContext delta)
        {
            if (_state == EInputState.Wait)
            {
                return;
            }
            if (delta.started != true)
                return;
            if (_state == EInputState.Player)
            {
                _state = EInputState.Inventory;
                _inventoryFacade.Open();
                return;
            }
            if (_state == EInputState.Inventory)
            {
                _state = EInputState.Player;
                _inventoryFacade.Close();
                _pantrySystemFacade.Close();
            }
            if (_state == EInputState.Cookbook)
            {
                _state = EInputState.Player;
                ToggleCookbook(false);
            }
        }
        public void ToggleInput(bool param)
        {
            if (param == false)
            {
                _laststate = _state;
                _state = EInputState.Wait;
                
            }
            else 
            {
                _state = _laststate;                
            }

        }
        public void ToggleCookbook(bool param)
        {
            if (param)
            {
                _cookbookSystemFacade.Open();
                _inventoryFacade.Close();
                _state = EInputState.Cookbook;
            }
            else
            {
                _cookbookSystemFacade.Close();
            }
        }       

        public void Init()
        {
            _cookbookSystemFacade.StateCookbook.Subscribe(_ =>  ToggleCookbook(true) );
        }
    }
}