using System;
using System.Linq;
using Underlings.Dumbwaiter;
using Underlings.Enum;
using Underlings.Items;
using Underlings.UISystem;
using UniRx;
using UnityEngine;
public class DumbSystemController : MonoBehaviour
{
    [SerializeField]
    private RecipeSystemFacade _recipeSystemFacade;
    [SerializeField]
    private CouldronController _couldronController;
    [SerializeField]
    private UIFacade _uIFacade;
    [SerializeField]
    private DumbwaiterStates _dumbwaiterStates;
    [SerializeField]
    private LifeSystemFacade _lifeSystemFacade;
    private Dish _currentDish;
    private Subject<EUSystemType> _onReady = new Subject<EUSystemType>();
    private Subject<EUSystemType> _onReload = new Subject<EUSystemType>();
    public bool TryGive(Item param)
    {
        return param.id == "DishReady";
        
    }
    public void Give(Item param)
    {
        var outcome = Outcome();
        var message = new UIMessage();
        message.Message = GetMessage(outcome);
        _uIFacade.ShowPanel(message);
        if (outcome == ERecipeValidation.Failed)
        {
            _onReload.OnNext(EUSystemType.DumbWaiter);
        }
        else
        {
            _onReady.OnNext(EUSystemType.DumbWaiter);
        }
    }

    public ERecipeValidation Outcome()
    {
        var ret = _recipeSystemFacade.ValidateRecipe(_currentDish.Id, _couldronController._recipeReady);
        _lifeSystemFacade.ToggleLife(ret);
        return ret;
        
    }

    private string GetMessage (ERecipeValidation param)
    {
        if (param == ERecipeValidation.Perfect)
        {
            return  _currentDish.PerfectMessage;
        }
        if (param == ERecipeValidation.Failed)
        {
            return  _currentDish.FailedMessage;
        }
        if (param == ERecipeValidation.OnlyEssential)
        {
            return  _currentDish.OnlyEssentialMessage;
        }
        return null;
    }
    public IObservable<Unit> LoadState(int state)
    {
        return Observable.Create<Unit>(obs =>
        {
            if (_dumbwaiterStates.States.All(x => x.FSMState != state))
            {
                _onReady.OnNext(EUSystemType.DumbWaiter);
            }
            else
            {
                _currentDish = _dumbwaiterStates.States.FirstOrDefault(x => x.FSMState == state)?.Dish;
            }

            obs.OnNext(Unit.Default);
            obs.OnCompleted();
            return Disposable.Empty;
        });
    }
    public IObservable<EUSystemType> Ready() => _onReady.AsObservable();
    public IObservable<EUSystemType> Reload() => _onReload.AsObservable();
}