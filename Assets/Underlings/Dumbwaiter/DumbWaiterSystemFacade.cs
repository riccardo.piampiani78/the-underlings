using System;
using Underlings.Enum;
using Underlings.Interfaces;
using Underlings.Items;
using UniRx;
using UnityEngine;

public class DumbWaiterSystemFacade : MonoBehaviour, IUnderlingsGivable,IUnderlingsSystem
{
    
    
    [SerializeField]
    private DumbSystemController _dumbWaiterSystemController;

    public bool TryGive(Item param) 
    {
       return _dumbWaiterSystemController.TryGive(param); 
    }
    public void Give(Item param)
    {
        _dumbWaiterSystemController.Give(param); 

    }
    public void Interact(Item param)
    {
       
    }
    public EUSystemType GetType()
    {
        return EUSystemType.DumbWaiter;
    }
    public IObservable<Unit> Init()
    {
        return Observable.Empty<Unit>();
    }
    public IObservable<Unit> LoadState(int state)
    {
        return _dumbWaiterSystemController.LoadState(state);
    }
    public IObservable<EUSystemType> Ready()
    {
        return _dumbWaiterSystemController.Ready();
    }
    public IObservable<EUSystemType> Reload()
    {
        return _dumbWaiterSystemController.Reload();
    }
}