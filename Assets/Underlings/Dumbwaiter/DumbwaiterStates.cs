﻿using System;
using System.Collections.Generic;
using UnityEngine;
namespace Underlings.Dumbwaiter
{
    [Serializable]
    public class Dish
    {
        public string Id;
        public string PerfectMessage;
        public string OnlyEssentialMessage;
        public string FailedMessage;
    }

    [Serializable]
    public class DumbwaiterState
    {
        public int FSMState;
        public Dish Dish;
    }
    
    [CreateAssetMenu(fileName = "DumbwaiterStates", menuName = "Underlings/DumbwaiterStates")]
    public class DumbwaiterStates : ScriptableObject
    {
        public List<DumbwaiterState> States = new List<DumbwaiterState>();
    }
}