
using Underlings.Interfaces;

using UnityEngine;


public class PantrySystemFacade : MonoBehaviour, IUnderlingsInteractable
{
    [SerializeField]
    private PantrySystemController _pantrySystemController;
    
    public void Interact()
    {
        _pantrySystemController.Interact();
    }

    public void Init()
    {

        _pantrySystemController.Init();
    }
    public void Close()
    {
        _pantrySystemController.Close();
    }


}

