using System.Collections.Generic;
using System.Linq;
using Underlings.Enum;
using Underlings.Input;
using Underlings.Inventory;
using Underlings.Items;
using UniRx;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class PantrySystemController : MonoBehaviour
{
    [SerializeField]
    private PantrySystemView _pantrySystemView;
    [SerializeField]
    private InventoryFacade _inventoryFacade;
    [SerializeField]
    private InputSystemController _inputSystemController;
    [SerializeField]
    private GameObject _pantryPanel;
    private Dictionary<string, GameObject> _items = new Dictionary<string, GameObject>();
    private Dictionary<string, Item> _models = new Dictionary<string, Item>();

    [SerializeField]
    private Button _nextPage;
    [SerializeField]
    private Button _previousPage;
    [SerializeField]
    private GameObject _gridPrefab;
    [SerializeField]
    private Transform _anchorTrasformItem;

    //resta da fare il paging del pantry
    //il check di non passargli tool?
    private int _currentPage = 0;

    public void Init()
    {
        _inventoryFacade.OnItemSelected.Subscribe(x => OnAddedItem(x));

        _nextPage.onClick.AddListener(NextPage);
        _previousPage.onClick.AddListener(PreviousPage);        
              
    }

    public void OnAddedItem(Item param)
    {
        if (_pantryPanel.activeSelf)
        {
            AddItem(param);
        }
    }
    public void Interact()
    {
        _inventoryFacade.gameObject.SetActive(true);
        _inputSystemController._state = EInputState.Inventory;
        _pantryPanel.SetActive(true);
    }

    public void AddItem(Item item)
    {
       if (_models.ContainsKey(item.id))
        {
            return;
        }       
      
        _models.Add(item.id,item);
      
        RefreshPage();

    }

    public void Close()
    {
        _pantryPanel.SetActive(false);
    }

    public void OnPressed(Item param)
    {
        if (_inventoryFacade.CanCollect(param))
        {
            _inventoryFacade.Collect(param);
        }

    }

    private void NextPage()
    {
        if (_currentPage< (_models.Count-1)/4)
            {
            _currentPage++;
            RefreshPage();
        }
        else
        {
            Debug.Log("non ho girato pagina");
        }

    }

    private void PreviousPage()
    {
        if(_currentPage!=0) { 
            
            _currentPage--;
            RefreshPage();

        }
        else
        {
            Debug.Log("non ho girato pagina");
        }
    }

    private void RefreshPage()
    {
        foreach (var item in _items) {
            Destroy(item.Value);
        }
        _items.Clear();
        var numberOfElement = _models.Count - _currentPage * 4;
        if (numberOfElement > 4)
        {
            numberOfElement = 4;
        }
        var m = _models.Select(x => x.Value).ToList().GetRange(_currentPage*4, numberOfElement);
        foreach (var item in m)
        {
            var p = Instantiate(_gridPrefab);
            var f = p.GetComponent<GridItemFacade>();
            p.transform.parent = _anchorTrasformItem;
            f.BindModel(item);
            _items.Add(item.id, p);
            f.OnPress().Subscribe(OnPressed);
        }
    }
}