using System;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class CountdownTimerFacade : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI timerText;
    private IDisposable timerSubscription;
    private Subject<Unit> _onTimeOut = new Subject<Unit>();
    public IObservable<Unit> OnTimeOut => _onTimeOut.AsObservable();
    public void StartTimer(int sec)
    {
        // Timer di 10 secondi con aggiornamento ogni secondo
        timerSubscription = Observable.Interval(System.TimeSpan.FromSeconds(1))
            .Take(sec) // Prende solo i primi sec 
            .Subscribe(x => { },
                () =>
                {
                    // Azione da eseguire allo scadere del timer
                    Debug.Log($"Il timer di {sec} secondi � scaduto.");
                    _onTimeOut.OnNext(Unit.Default);
                    // Smaltisce la sottoscrizione
                    timerSubscription.Dispose();
                }
            );
      
    }

   /* private void secondsElapsed(long param)
    {
        // Aggiorna il testo con i secondi trascorsi
        timerText.text = (param + 1).ToString();
        Debug.Log("Secondi trascorsi: " + (param + 1));
    }*/
}