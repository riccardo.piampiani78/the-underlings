using System;
using Underlings.Items;
using UnityEngine;
namespace Underlings.Inventory
{
    public class GridItemFacade : MonoBehaviour
    {
        [SerializeField]
        private GridItemController _controller;
        public IObservable<Item> OnPress()
        {
            return _controller.OnPress();
        }
        public void BindModel (Item item)
        {
            _controller.BindModel(item);
        }
    }
}