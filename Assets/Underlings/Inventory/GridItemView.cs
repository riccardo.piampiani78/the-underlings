using System;
using Underlings.Items;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
namespace Underlings.Inventory
{
    public class GridItemView : MonoBehaviour
    {
        [SerializeField]
        private Image _image;
        [SerializeField]
        private Button _button;
        private Subject<Item> _onPress = new Subject<Item>();
        public IObservable<Item> OnPress() => _onPress.AsObservable();
        private Item _item;

    
        public void Pressed()
        {
            _onPress.OnNext(_item);
        }
        public void BindModel(Item item)
        {
            _image.sprite = item.Icon;
            _button.onClick.RemoveListener(Pressed);
            _button.onClick.AddListener(Pressed);
            _item = item;
        }
    }
}