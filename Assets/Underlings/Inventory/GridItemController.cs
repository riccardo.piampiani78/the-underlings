using System;
using Underlings.Items;
using UnityEngine;
namespace Underlings.Inventory
{
    public class GridItemController : MonoBehaviour
    {
        [SerializeField]
        private GridItemView _view;

        public IObservable<Item> OnPress()
        {
            return _view.OnPress();
        }
        public void BindModel(Item item)
        {
            _view.BindModel(item);
        }
    }
}