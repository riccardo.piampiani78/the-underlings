﻿using System;
using System.Collections.Generic;
using System.Linq;
using Underlings.Couldron;
using Underlings.Enum;
using Underlings.UISystem;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Item = Underlings.Items.Item;

namespace Underlings.Inventory
{
    public class InventoryController : MonoBehaviour
    {
        private int _maxItem = 6;
        private Dictionary<string, GameObject> _items = new Dictionary<string, GameObject>();
        private Dictionary<string, GameObject> _tools = new Dictionary<string, GameObject>();
        [SerializeField]
        private Button _toolsButton;
        [SerializeField]
        private Button _ingredientsButton;
        [SerializeField]
        private Transform _anchorTrasformItem;
        [SerializeField]
        private Transform _anchorTrasformTool;
        [SerializeField]
        private GameObject _gridPrefab;
        [SerializeField]
        private CouldronFacade _couldronFacade;
        [SerializeField]
        private Item _dishReady;
        [SerializeField]
        private InventoryStates _inventoryStates;
        [SerializeField]
        private UIFacade _uiFacade;
        [SerializeField]
        private Transform _playerTransform;

        private int _currentState;
        private Subject<EUSystemType> _onReady = new Subject<EUSystemType>();
        private Subject<Item> _onItemSelected = new Subject<Item>();
        private Subject<Item> _onItemCollected = new Subject<Item>();
        public IObservable<Item> OnItemCollected => _onItemCollected.AsObservable();
        public IObservable<Item> OnItemSelected => _onItemSelected.AsObservable();
        public bool CanCollect(Item model)
        {
            if (_maxItem >= _items.Count || model.IsTool)
            {
                return true;
            }
            return false;
        }

        internal void Close()
        {
            gameObject.SetActive(false);
        }

        public void Collect(Item item)
        {
            if (item.IsTool)
            {
                AddTool(item);
            }
            else
            {
                AddItem(item);
            }

            if (item.ShowMessage)
            {
                var message = new UIMessage();
                message.Message = item.MessageOnPick;
                message.PanelType = EUPanelType.Player;

                if (item.isPopUp)
                {
                    _uiFacade.ShowPanelOnTarget(_playerTransform, message).Subscribe();
                }

                else
                {
                    _uiFacade.ShowPanel(message);
                }
            }

            CheckState();
        }
        public void AddItem(Item item)
        {
            if (_items.ContainsKey(item.id))
            {
                return;
            }
            _onItemCollected.OnNext(item);
            var p = Instantiate(_gridPrefab);
            var f = p.GetComponent<GridItemFacade>();
            p.transform.parent = _anchorTrasformItem;
            f.BindModel(item);
            f.OnPress().Subscribe(OnPressed);
            _items.Add(item.id, p);

        }

        public void AddTool(Item item)
        {
            if (_tools.ContainsKey(item.id))
            {
                return;
            }
            var p = Instantiate(_gridPrefab);
            var f = p.GetComponent<GridItemFacade>();
            p.transform.parent = _anchorTrasformTool;
            f.BindModel(item);
            f.OnPress().Subscribe(OnPressed);

            _tools.Add(item.id, p);
        }
        private void OnPressed(Item item)
        {
            Debug.Log($"hai selezionato {item.Name}");
            _onItemSelected.OnNext(item);
        }
        internal void Open()
        {
            gameObject.SetActive(true);
        }

        public void RemoveItem(Item param)
        {
            if (param.id == "DishReady")
            {
                var gotool = _tools[param.id];
                Destroy(gotool);
                _tools.Remove(param.id);
            }
            else
            {
                var go = _items[param.id];
                Destroy(go);
                _items.Remove(param.id);
            }
        }

        public IObservable<Unit> Init()
        {

            return Observable.Create<Unit>(obs =>
            {
                _toolsButton.onClick.AddListener(setTools);
                _ingredientsButton.onClick.AddListener(setIngredients);
                _couldronFacade.OnDishReady().Subscribe(OnDishReady);
                obs.OnNext(Unit.Default);
                obs.OnCompleted();
                return Disposable.Empty;
            });


        }

        private void OnDishReady(Unit unit)
        {
            AddTool(_dishReady);
        }

        private void setIngredients()
        {
            setButtonColorMultiplayer(_toolsButton, 1.0f);
            setButtonColorMultiplayer(_ingredientsButton, 2.5f);
            _anchorTrasformItem.gameObject.SetActive(true);
            _anchorTrasformTool.gameObject.SetActive(false);
        }

        private void setTools()
        {
            setButtonColorMultiplayer(_toolsButton, 2.5f);
            setButtonColorMultiplayer(_ingredientsButton, 1f);
            _anchorTrasformItem.gameObject.SetActive(false);
            _anchorTrasformTool.gameObject.SetActive(true);
        }

        private void setButtonColorMultiplayer(Button b, float cm)
        {
            var colors = b.colors;
            colors.colorMultiplier = cm;
            b.colors = colors;
        }

        internal IObservable<EUSystemType> Ready()
        {
            return _onReady.AsObservable();
        }

        internal IObservable<EUSystemType> Reload()
        {
            return Observable.Empty<EUSystemType>();
        }

        internal IObservable<Unit> LoadState(int state)
        {
            return Observable.Create<Unit>(obs =>
            {
                _currentState = state;
                if (_inventoryStates.States.All(x => x.FSMState != state))
                {
                    _onReady.OnNext(EUSystemType.InventorySystem);
                }

                obs.OnNext(Unit.Default);
                obs.OnCompleted();
                return Disposable.Empty;
            });
        }

        public EUSystemType GetType()
        {
            return EUSystemType.InventorySystem;
        }

        private void CheckState()
        {
            var items = _inventoryStates.States.Where(x => x.FSMState == _currentState);
            if (!items.Any()) return;
            foreach (var item in items.First().ObjectIds)
            {
                if (!_items.ContainsKey(item) && (!_tools.ContainsKey(item)))
                    return;

            }

            _onReady.OnNext(EUSystemType.InventorySystem);
        }


    }
}