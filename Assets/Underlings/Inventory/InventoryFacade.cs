using System;
using Underlings.Enum;
using Underlings.Interfaces;
using Underlings.Items;
using UniRx;
using UnityEngine;
namespace Underlings.Inventory
{
    public class InventoryFacade : MonoBehaviour, IUnderlingsSystem
    {
       
        [SerializeField]
        private InventoryController _inventoryController;


        public IObservable<Item> OnItemSelected => _inventoryController.OnItemSelected;
        public IObservable<Item> OnItemCollected => _inventoryController.OnItemCollected;



        public void Open() {

            _inventoryController.Open();

        }
        public void Close()
        {

            _inventoryController.Close();

        }

        public bool CanCollect(Item model)
        {
            return _inventoryController.CanCollect(model);
        }

        public void Collect (Item item) 
        {
            _inventoryController.Collect(item);
        }

        public void RemoveItem(Item param)
        {
            _inventoryController.RemoveItem(param);
        }

        public EUSystemType GetType()
        {
            return _inventoryController.GetType();
        }

        public IObservable<Unit> Init()
        {
            return _inventoryController.Init();
        }

        public IObservable<Unit> LoadState(int state)
        {
            return _inventoryController.LoadState(state);
        }

        public IObservable<EUSystemType> Ready()
        {
            return _inventoryController.Ready();
        }

        public IObservable<EUSystemType> Reload()
        {
            return _inventoryController.Reload();
        }
    }
}