using System.Collections.Generic;
using System;
using UnityEngine;

[Serializable]
public class Inventory
{
    public int FSMState;
    public List<string> ObjectIds = new List<string>();
}

[CreateAssetMenu(fileName = "InventoryStates", menuName = "Underlings/InventoryStates")]
public class InventoryStates : ScriptableObject
{
    public List<Inventory> States = new List<Inventory>();
}