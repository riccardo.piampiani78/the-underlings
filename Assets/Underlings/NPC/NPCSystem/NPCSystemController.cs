using System;
using Underlings.Enum;
using Underlings.Interfaces;
using Underlings.Items.ItemWorld;
using Underlings.Items;
using UniRx;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Underlings.Inventory;
using Underlings.LevelSystem;
using Underlings.UISystem;
using Underlings.Input;

public class NPCSystemController : MonoBehaviour
{
    [SerializeField]
    private NPCWorldFacade _NPCworldFacade;
    [SerializeField]
    private NPCStates _npcState;
    private Subject<EUSystemType> _onReady = new Subject<EUSystemType>();
    [SerializeField]
    private LevelSystemFacade _levelSystemFacade;
    private Dictionary<string,NPCWorldFacade> NPCs = new Dictionary<string,NPCWorldFacade>();
    
    //Per gli npcWorld
    [SerializeField]
    private UIFacade _ui; 
    [SerializeField]
    private InventoryFacade _inventoryFacade;
    [SerializeField]
    private ItemDirectory _itemDirectory;
    [SerializeField]
    private InputSystemFacade _inputSystemFacade;

    private Dictionary<string, bool> _npcReady = new Dictionary<string, bool>();

    public IObservable<Unit> LoadState(int state)
    {
        return Observable.Create<Unit>(obs =>
        {
            bool StateFound = false;
            foreach (var npc in _npcState.NPC)
            {
                if (npc.FSMState == state)
                {
                    StateFound = true;
                    foreach (var npc1 in npc.NPC)
                    {
                        if (npc1.state == ENPCState.Spawn)
                        {
                            var InstantiatedPrefab = InstantiatePrefab(npc1);
                            var Parent = _levelSystemFacade.GetSpawnPointTransform(npc1.idroom, npc1.SpawnPoint);
                            InstantiatedPrefab.transform.SetParent(Parent);
                            InstantiatedPrefab.transform.localPosition = new Vector3(0, 0, 0);
                            //Attento a quando danno il primo ready và aggiunto l'init
                            
                            NPCs.Add(npc1.idNPC, InstantiatedPrefab);
                        }
                        if (npc1.state == ENPCState.Update)
                        {
                            NPCs[npc1.idNPC].BindModel(npc1);
                        }
                    }
                }
            }
            if (StateFound == false)
            {
                _onReady.OnNext(EUSystemType.NPCSystem);
            }
            obs.OnNext(Unit.Default);
            obs.OnCompleted();
            return Disposable.Empty;
            
        });
    }
    private void HandleNpcReady(string IDNpc)
    {
        if (_npcReady.ContainsKey(IDNpc))
        {
            _npcReady[IDNpc] = true;
            if (_npcReady.All(x => x.Value))
            {
                _onReady.OnNext(EUSystemType.NPCSystem);
            }
        }
    }

    public NPCWorldFacade InstantiatePrefab(NPC NPC)
    {
        var p = Instantiate(_NPCworldFacade);  //crea il prefab a schermo
        p.SetReferences(_levelSystemFacade,_itemDirectory,_inventoryFacade,_ui, _inputSystemFacade);
        _npcReady.Add(NPC.idNPC,false);
        p.Onready().Subscribe(HandleNpcReady);
        p.BindModel(NPC); //personalizza il prefab con il modello
        
        return p;
    }

    public IObservable<EUSystemType> Ready()
    {
        return _onReady.AsObservable();
    }
    public IObservable<EUSystemType> Reload()
    {
        return Observable.Empty<EUSystemType>();
    }
    public EUSystemType GetType()
    {
        return EUSystemType.NPCSystem;
        
    }

    public IObservable<Unit> Init()
    {
        return Observable.Empty<Unit>(); 
    }
}