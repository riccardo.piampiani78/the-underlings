using System;
using Underlings.Enum;
using Underlings.Interfaces;
using UniRx;
using UnityEngine;

public class NPCSystemFacade : MonoBehaviour, IUnderlingsSystem
{
    [SerializeField] 
    private NPCSystemController _controller;
    public IObservable<Unit> Init()
    {
        return _controller.Init();
    }

    public IObservable<Unit> LoadState(int state)
    {
       return _controller.LoadState(state);

    }

    public IObservable<EUSystemType> Ready()
    {
       return _controller.Ready();
    }

    public IObservable<EUSystemType> Reload()
    {
        return _controller.Reload();
    }

    EUSystemType IUnderlingsSystem.GetType()
    {
        
        return _controller.GetType();
    }
}