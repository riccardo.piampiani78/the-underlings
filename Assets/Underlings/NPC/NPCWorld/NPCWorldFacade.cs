using System;
using System.Collections;
using System.Collections.Generic;
using Underlings.Input;
using Underlings.Interfaces;
using Underlings.Inventory;
using Underlings.Items;
using Underlings.LevelSystem;
using Underlings.UISystem;
using UniRx;
using UnityEngine;

public class NPCWorldFacade : MonoBehaviour,IUnderlingsInteractable,IUnderlingsGivable
{
    [SerializeField]
    private NPCWorldController _NPCWorldController;
    public IObservable<string> Onready() => _NPCWorldController.Onready();
    public void BindModel( NPC model)
    {
        _NPCWorldController.BindModel(model);
    }

    public void Interact()
    {
        _NPCWorldController.Interact();
    }
    public bool TryGive(Item param)
    {
        return _NPCWorldController.TryGive(param);
    }
    public void Give(Item param)
    {
        _NPCWorldController.Give(param);
    }
    public void Interact(Item param)
    {
        _NPCWorldController.Interact();
    }
    public void SetReferences(LevelSystemFacade lvl,ItemDirectory itmd,InventoryFacade invFacade,UIFacade ui, InputSystemFacade input)
    {
        _NPCWorldController.SetReferences(lvl,itmd,invFacade,ui, input);
    }

}
