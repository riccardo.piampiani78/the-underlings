using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using Unity.VisualScripting;
using UnityEngine;
using Unit = UniRx.Unit;

public class NPCWorldView : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer _spriteRenderer;

    [SerializeField]
    private Animator _animator;
    public void BindModel(NPC model)
    {
        if (model.IdleAnimation == null)
        {
            Debug.LogError("Attento animazione nulla");
        }
        PlayAnimationClip(model.IdleAnimation);
    }

    private void PlayAnimationClip(AnimationClip clip)
    {
        var animatorOverrideController = new AnimatorOverrideController(_animator.runtimeAnimatorController);
        _animator.runtimeAnimatorController = animatorOverrideController;
        animatorOverrideController["Player_idle"] = clip;
        _animator.Play("AnimationToPlay", -1, 0f);
    }

    public IObservable<Unit> PlayAnimationOBS(AnimationClip clip)
    {
        return Observable.Create<Unit>(observer =>
        {
            // Chiama il metodo per sostituire e riprodurre l'animazione.
            PlayAnimationClip(clip);

            // Calcola la durata dell'animazione.
            float animationDuration = clip.length;

            // Aspetta che l'animazione finisca prima di inviare le notifiche.
            IDisposable subscription = Observable.Timer(TimeSpan.FromSeconds(animationDuration))
                .Subscribe(_ =>
                {
                    observer.OnNext(Unit.Default); // Notifica l'osservatore che l'animazione è finita.
                    observer.OnCompleted(); // Completa l'observable.
                });

            // Restituisce una funzione di cleanup che verrà chiamata quando l'observable viene disposto.
            return Disposable.Create(() =>
            {
                subscription.Dispose();
            });
        });
    }
}