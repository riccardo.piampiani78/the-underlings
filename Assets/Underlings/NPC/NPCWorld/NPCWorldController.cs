using System;
using System.Collections.Generic;
using System.Linq;
using Underlings.Enum;
using Underlings.Input;
using Underlings.Inventory;
using Underlings.Items;
using Underlings.LevelSystem;
using Underlings.UISystem;
using UniRx;
using UnityEngine;
public class NPCWorldController : MonoBehaviour
{
    private Transform _playerTransform;
    [SerializeField]
    NPCWorldView _NPCWorldView;
    [SerializeField]
    private InputSystemFacade _InputSystemFacade;

    private NPC _model;
    
    private UIFacade _ui; 
    private InventoryFacade _inventoryFacade;
    private ItemDirectory _itemDirectory;
    private LevelSystemFacade _levelSystemFacade;
    private IDisposable _dispAnimation;
    private List<NPCAction> _actions = new List<NPCAction>();
    private Subject<string> _onReady = new Subject<string>();
    public IObservable<string> Onready() => _onReady;
    private CompositeDisposable _compositeDisposable = new CompositeDisposable();
    public void SetReferences(LevelSystemFacade lvl,ItemDirectory itmd,InventoryFacade invFacade,UIFacade ui, InputSystemFacade input )
    {
        _levelSystemFacade = lvl;
        _itemDirectory = itmd;
        _inventoryFacade = invFacade;
        _InputSystemFacade = input;
        _ui = ui;
    }
    public void BindModel(NPC model)
    {
        _compositeDisposable?.Dispose();
        _compositeDisposable = new CompositeDisposable();
        if (_playerTransform == null)
        {
            _playerTransform = GameObject.Find("Player").GetComponent<Transform>();
        }
        _model = model;
        _actions.Clear(); //Qui cancella tutte le azioni non compiute nelle fasi precedenti... (Da valutare)
        foreach (var act in _model.Actions)
        {
            var a = new NPCAction();
            a.Clone(act);
            _actions.Add(a);
        }
        
        _NPCWorldView.BindModel(model);
        ExecuteAction(_actions.First());
        _levelSystemFacade.OnPlayerEnterInRoom().Subscribe(HandlePlayerEnterInARoom).AddTo(_compositeDisposable);
        if (_model.Actions.Count(x => x.Type == ENPCActionType.WaitForPlayerInteraction) < 1)
        {
            _onReady.OnNext(_model.idNPC);
        }
    }
    public void Interact()
    {
        ExecuteAction(_actions.First());
    }

    private void ExecuteAction(NPCAction action)
    {
        switch (action.Type)
        {
            case ENPCActionType.Text:
            {
                ShowMessage(action.Text, action.MessageType);
                GoToNextAction();
            }
                break;
            case ENPCActionType.Get:
            {
                _inventoryFacade.Collect(_itemDirectory.GetItemById(action.IdToGet));
                ShowMessage(action.Text, action.MessageType);
                GoToNextAction();
            }
                break;
            case ENPCActionType.Give:
            {
                ShowMessage(action.Text, action.MessageType);
            }
                break;
            case ENPCActionType.PlayerEnterRoom:
                if (_levelSystemFacade.GetPlayerPositionRoomId() == action.IdRoom)
                {
                    GoToNextAction();
                }
                break;
            case ENPCActionType.Animation:
            {
                    if (action.StopInputPlayer) {

                        _InputSystemFacade.ToggleInput(false);
                    }
                _dispAnimation?.Dispose();
                _dispAnimation = _NPCWorldView
                    .PlayAnimationOBS(action.AnimationClip)
                    .DoOnCompleted(() =>
                    {
                        if (action.StopInputPlayer) { _InputSystemFacade.ToggleInput(true); }
                        if (!action.LoopAnimation)
                        {
                            _dispAnimation?.Dispose();
                            _NPCWorldView
                                .PlayAnimationOBS(_model.IdleAnimation)
                                .Subscribe().AddTo(_compositeDisposable);
                        }
                        GoToNextAction();
                    })
                    .Subscribe();
            }
                break;
            case ENPCActionType.WaitForPlayerInteraction:
            {
                _onReady.OnNext(_model.idNPC);
            }   
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        
    }
    public bool TryGive(Item item)
    {
        var ret = item.id == _actions.First().IdToGive;
        if(!ret)
            ShowMessage(_actions.First().ErrorText, _actions.First().MessageType);
        return ret;
    }
    
    public void Give(Item item)
    {
        if (item.id == _actions.First().IdToGive)
        {
            GoToNextAction();
        }
    }

    private void GoToNextAction()
    {
        if (_actions.Count < 2)
            return;
        _actions.RemoveAt(0);
        if (!_actions.First().AutoExecute) return;
        ExecuteAction(_actions.First());
    }

    private void ShowMessage(string msgT,EUPanelType panelType)
    {
        var msg = new UIMessage
        {
            Message = msgT,
            PanelType = panelType
        };
        var t = panelType == EUPanelType.Player ? _playerTransform : transform;
        _ui.ShowPanelOnTarget(t,msg)
            .Subscribe().AddTo(_compositeDisposable);
    }

    private void HandlePlayerEnterInARoom(string room)
    {
        if (_actions.First().Type != ENPCActionType.PlayerEnterRoom) return;
        if (room != _actions.First().IdRoom) return;
        GoToNextAction();
    }


}