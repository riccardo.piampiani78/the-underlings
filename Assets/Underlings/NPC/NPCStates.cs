using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Underlings.Enum;
using UnityEngine;
using UnityEngine.Serialization;


[Serializable]
public class NPCAction
{
    public ENPCActionType Type;
    [ShowIf("ShowAutoExecute")]
    [Tooltip("Se questo flag è settato, questa azione sarà effettuata automaticamente")]
    public bool AutoExecute;
    [ShowIf("ShowText")]
    public string Text;
    [ShowIf("ShowIdToGive")]
    public string ErrorText;
    [ShowIf("ShowText")]
    public EUPanelType MessageType;
    [ShowIf("ShowIdToGet")]
    public string IdToGet;
    [ShowIf("ShowIdToGive")]
    public string IdToGive;
    [ShowIf("ShowIdRoom")]
    public string IdRoom;
    [ShowIf("ShowAnimationClip")]
    public bool StopInputPlayer;

    [ShowIf("ShowAnimationClip")]
    public AnimationClip AnimationClip;
    [ShowIf("ShowAnimationClip")]
    [Tooltip("Se questo flag è settato, l'animazione verrà ripetuta invece che tornare in IDLE")]
    public bool LoopAnimation;
    
    private bool ShowText() { return Type == ENPCActionType.Text || 
                                     Type == ENPCActionType.Get ||
                                     Type == ENPCActionType.Give; 
    }
    private bool ShowIdToGet() { return Type == ENPCActionType.Get; }
    private bool ShowIdToGive() { return Type == ENPCActionType.Give; }
    private bool ShowIdRoom() { return Type == ENPCActionType.PlayerEnterRoom; }
    private bool ShowAnimationClip() { return Type == ENPCActionType.Animation; }
    private bool ShowAutoExecute() { return Type != ENPCActionType.Get && 
                                            Type != ENPCActionType.Give && 
                                            Type != ENPCActionType.PlayerEnterRoom; }
    public void Clone(NPCAction toClone)
    {
        Type = toClone.Type;
        AutoExecute = toClone.AutoExecute;
        Text = toClone.Text;
        ErrorText = toClone.ErrorText;
        MessageType = toClone.MessageType;
        IdToGet = toClone.IdToGet;
        IdToGive = toClone.IdToGive;
        IdRoom = toClone.IdRoom;
        AnimationClip = toClone.AnimationClip;
        LoopAnimation = toClone.LoopAnimation;
        StopInputPlayer = toClone.StopInputPlayer;
    }
}


[Serializable]
public class NPC
{
    public string SpawnPoint;
    public string idroom;
    public string idNPC;
    public  ENPCState state;
    public AnimationClip IdleAnimation;
    public List<NPCAction> Actions = new List<NPCAction>();
}

[Serializable]
public class NPCState
{
   public int FSMState;
   public List<NPC> NPC = new List<NPC>();
}

[CreateAssetMenu(fileName = "NewNPCState", menuName = "Underlings/CreateNPCState")]
public class NPCStates : ScriptableObject
{
    public List<NPCState> NPC = new List<NPCState>();
} 