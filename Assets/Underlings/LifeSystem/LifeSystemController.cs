﻿using System;
using System.Collections.Generic;
using System.Linq;
using Underlings.Enum;
using UnityEngine;

public class LifeSystemController : MonoBehaviour
{
    [SerializeField]
    private LifeSystemView _lifeSystemView;
    
    [SerializeField]
    private GameObject Life;
    private List<GameObject>_lifes = new List<GameObject>();
    [SerializeField]
    private int InitialLife;
    [SerializeField]
    private int MaximumLife;

    private void InstantiateLife()
    {
        if (_lifes.Count < MaximumLife)
        {
            var p = Instantiate(Life);
            p.transform.SetParent(transform);
            _lifes.Add(Life);
        }
    }

    private void RemoveLife()
    {
        DestroyImmediate(_lifes.First(),true);
        _lifes.RemoveAt(0);
    }
    public void SetInitialLife()
    {
        for(int i = 0; i<InitialLife;i++)
        {
            InstantiateLife();
        }
    }    


    public void ToggleLife(ERecipeValidation outcome)
    {
        if (outcome == ERecipeValidation.Perfect)
        {
            InstantiateLife();
        }
        if (outcome == ERecipeValidation.Failed)
        {
            RemoveLife();
        }
      //  _lifeSystemView.ToggleLife(outcome);
    }
}