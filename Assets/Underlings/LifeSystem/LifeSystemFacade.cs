using System;
using Underlings.Enum;
using UnityEngine;
public class LifeSystemFacade : MonoBehaviour
{    
    [SerializeField]
    private LifeSystemController _lifeSystemController;

    public void ToggleLife(ERecipeValidation outcome) 
    {
        _lifeSystemController.ToggleLife(outcome);

    }

    internal void Init()
    {
        _lifeSystemController.SetInitialLife();
    }
}