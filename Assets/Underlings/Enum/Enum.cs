namespace Underlings.Enum
{
    public enum EUSystemType{
        UI,
        Player,
        Level,
        ItemSystem,
        NPCSystem,
        InventorySystem,
        DumbWaiter
    }

    public enum EUPanelType
    {
        Player,
        Guard,
        King

    }

    public enum EUDoorState
    {
        Locked,
        Opened,
        Closed
    }

    public enum EInputState
    {
        Player, //Stato di base
        Inventory, //Stato inventario
        Wait,  //stato di attesa del sistema
        Cookbook    //stato lettura ricettario

    }

    public enum ERecipeValidation
    {
        Perfect,
        OnlyEssential,
        Failed
    }
    public enum ENPCState
    {
        Spawn, 
        Update,
        Replace
    }

    public enum ENPCActionType
    {
        Text, //Il messaggio che mostra a schermo
        Get, //Possibilità di dare l'oggetto all'npc o la ricetta pronta
        Give, //Possibilità di prendere l'oggetto dall'npc
        PlayerEnterRoom, //Aspetta che il player sia entrato dentro alla stanza
        Animation, //Riproduce l'animazione
        WaitForPlayerInteraction
    }
    
}