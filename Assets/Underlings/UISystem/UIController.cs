using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using Underlings.Enum;
using UniRx;
using UnityEngine;

namespace Underlings.UISystem
{
    public class UIController : MonoBehaviour
    {
        [SerializeField]
        private UIView _UIview;
        private Subject<EUSystemType> _onReload = new Subject<EUSystemType>();
        public IObservable<EUSystemType> Ready() => _UIview.OnclosedPanels;
        public IObservable<EUSystemType> Reload() => _onReload.AsObservable();

        private Dictionary<int, List<UIMessage>> _messageToShowAtLoadState = new Dictionary<int, List<UIMessage>>();

        public IObservable<Unit> Init()
        {
            return Observable.Create<Unit>(obs =>
            {
                var path = Path.Combine(Application.streamingAssetsPath, "Json", "UiMessages.json");
                ReadMessageToShow(path);
                obs.OnNext(Unit.Default);
                obs.OnCompleted();
                return Disposable.Empty;
            });
        }

        public void ReadMessageToShow(string path)
        {
            string json = File.ReadAllText(path);
            _messageToShowAtLoadState = JsonConvert.DeserializeObject<Dictionary<int, List<UIMessage>>>(json);
        }
        public IObservable<Unit> LoadState(int state)
        {
            return Observable.Create<Unit>(obs =>
            {
                //Controllo se ci sono pannelli da mostrare per questo stato
                if (_messageToShowAtLoadState.TryGetValue(state, out var value))
                {
                    _UIview.CreatePanels(value);
                }
                if (value == null)
                {
                    _UIview.Ready();
                }
                obs.OnNext(Unit.Default);
                obs.OnCompleted();
                return Disposable.Empty;
            });
        }
        /* public void ShowOutcome(ERecipeValidation param)
             {
                 if (param == ERecipeValidation.Perfect) 
                 {
                     textMeshProUGUI.text = "Era buonissimo Chef!";
                 }
                 if (param == ERecipeValidation.Failed)
                 {
                     textMeshProUGUI.text = "Cos'era questa schifezza?! Un'altro errore del genere e mangiero' te!";
                 }
                 if (param == ERecipeValidation.OnlyEssential)
                 {
                     textMeshProUGUI.text = "Mmm, non male, manca comunque qualcosa a questo piatto";
                 }
             }*/
        public void ShowPanel(UIMessage message)
        {
            _UIview.CreatePanels(new List<UIMessage>() { message });
        }
        public IObservable<Unit> ShowPanelOnTarget(Transform target, UIMessage message)
        {
            return _UIview.ShowPanelOnTarget(target, message);
        }

        public bool AreThereOpenPanels()
        {

            return _UIview.AreThereOpenPanels();
        }

        public bool GoToNextPanel()
        {
            return _UIview.GoToNextPanel();
        }
    }


}