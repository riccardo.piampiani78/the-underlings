using Underlings.Enum;
using UnityEngine;
using UnityEngine.UI;
namespace Underlings.UISystem
{
    public class UIMessage {

        public string Message;
        public string ImageName;
        public EUPanelType PanelType;
    }
}