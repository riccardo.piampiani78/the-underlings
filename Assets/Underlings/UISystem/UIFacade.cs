using System;
using Underlings.Enum;
using Underlings.Interfaces;
using UniRx;
using UnityEngine;
namespace Underlings.UISystem
{
    public class UIFacade : MonoBehaviour, IUnderlingsSystem
    {
        
        [SerializeField]
        private UIController _UIController;
        

        //Questo è il metodo che devi chiamare negli altri sistemi per far visualizzare un pannello.
        public void ShowPanel(UIMessage message)
        {
            _UIController.ShowPanel(message);
        }

        public IObservable<Unit> ShowPanelOnTarget(Transform target, UIMessage message)
        {
            return _UIController.ShowPanelOnTarget(target, message);
        }

        public IObservable<Unit> Init()
        {
            return _UIController.Init();
        }

        public IObservable<Unit> LoadState(int state)
        {
            return _UIController.LoadState(state);
        }

        public new EUSystemType GetType()
        {
            return EUSystemType.UI;
        }

        public IObservable<EUSystemType> Ready()
        {
            return _UIController.Ready();
        }

        public IObservable<EUSystemType> Reload()
        {
            return _UIController.Reload();
        }

        public bool AreThereOpenPanels()
        {

            return _UIController.AreThereOpenPanels();
        }
        
        
        public bool GoToNextPanel()
        {
            return _UIController.GoToNextPanel();
        }
    }
}