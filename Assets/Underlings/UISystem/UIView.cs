using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Underlings.Enum;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
namespace Underlings.UISystem
{

    [Serializable]
    public class PanelTypeSprite
    {
        [SerializeField]
        public EUPanelType Type;
        [SerializeField]
        public Sprite Sprite;
    }
    public class UIView : MonoBehaviour
    {
        [SerializeField]
        private List<PanelTypeSprite> _panelTypeSprites = new List<PanelTypeSprite>();
        [SerializeField]
        private TextMeshProUGUI _panelMessage;
        [SerializeField]
        private Image _panelImage;
        [SerializeField]
        private Image _backgroundImage;
        [SerializeField]
        private Button _InvisibleCloseButton;
        private List<UIMessage>  _messages = new List<UIMessage>();
        private Subject<EUSystemType> _onclosedPanels = new Subject<EUSystemType>();
        public IObservable<EUSystemType> OnclosedPanels => _onclosedPanels.AsObservable();
        private bool _isPanelOpened = false;
        [SerializeField]
        private TextMeshProUGUI _fumetto;
        [SerializeField]
        private GameObject _fumettoPanel;
        [SerializeField]
        private Button _fumettoButton;
        private Subject<Unit> _onButtonClose = new Subject<Unit>();
        [SerializeField]
        private Transform _fumettoTargetPosition;
        private RectTransform _canvasRectTransform;
        [SerializeField]
        private Vector2 _deltaFumettoPosition = new Vector2(0,0);
        [SerializeField]
        private ContentSizeFitter _contentSizeFitter;
        [SerializeField]
        private CountdownTimerFacade _countdownTimerFacade;
        private Color GetColorByType(EUPanelType panelType)
        {
            var col = Color.white;
            switch (panelType)
            {
                case EUPanelType.Player:
                    col =Color.cyan;
                    break;
                case EUPanelType.Guard:
                    col = Color.gray;
                    break; 
                case EUPanelType.King:
                    col = Color.magenta;
                    break;
            }
            
            return new Color(col.r,col.g,col.b,0.7f);
        }
        public void CreatePanels(List<UIMessage> panel)
        {
            _messages.AddRange(panel);
            if (!_isPanelOpened)
            {
                OpenPanel();
            }
        }
        private void OpenPanel()
        {
            if (_messages.Count == 0)
            {
                TogglePanel(false);
                _onclosedPanels.OnNext(EUSystemType.UI);
                return;
            }
        
            _panelMessage.text = _messages[0].Message;
            _panelImage.sprite = _panelTypeSprites.FirstOrDefault(x => x.Type == _messages[0].PanelType)?.Sprite;
            _backgroundImage.color = GetColorByType(_messages[0].PanelType);
            _messages.RemoveAt(0);
            _InvisibleCloseButton.onClick.RemoveAllListeners();
            _InvisibleCloseButton.onClick.AddListener(OpenPanel);
            TogglePanel(true);
            ForceUpdateContentSizeFitter();

        }
        
        void ForceUpdateContentSizeFitter()
        {
            // Forza l'aggiornamento del ContentSizeFitter
            _contentSizeFitter.SetLayoutHorizontal();
            _contentSizeFitter.SetLayoutVertical();
        
            // A volte potrebbe essere necessario forzare l'aggiornamento del layout in questo modo:
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)transform);
        }
        private void TogglePanel(bool toggle)
        {
            _panelMessage.gameObject.SetActive(toggle);
            _panelImage.gameObject.SetActive(toggle);
            _backgroundImage.gameObject.SetActive(toggle);
            _isPanelOpened = toggle;
        }

        private void ToggleFumetto(Unit unit)
        {
            _fumettoPanel.SetActive(false);
        }
      

        public IObservable<Unit> ShowPanelOnTarget(Transform target, UIMessage message)
        {
            return Observable.Create<Unit>(obs =>
            {
                _fumettoTargetPosition = target;
                _fumettoPanel.SetActive(true);
                _fumetto.text = message.Message;
                _fumettoButton.onClick.AddListener(HandleCloseButton);
                _countdownTimerFacade.StartTimer(5);
                _countdownTimerFacade.OnTimeOut.Subscribe(ToggleFumetto);
                return _onButtonClose.Subscribe(obs);
            });
        }

        private void HandleCloseButton()
        {
            _onButtonClose.OnNext(Unit.Default);
            _fumettoPanel.SetActive(false);
        }

        public void Start()
        {
            _canvasRectTransform = GetComponentInParent<Canvas>().GetComponent<RectTransform>();
        }
        public void Update()
        {
            if (_fumettoPanel.activeSelf && _fumettoTargetPosition != null)
            {
                var cam = UnityEngine.Camera.main;
                Vector2 screenPoint = cam.WorldToScreenPoint(_fumettoTargetPosition.position);
                ((RectTransform)_fumettoPanel.transform).position = screenPoint + _deltaFumettoPosition;
            }
        }
        public bool AreThereOpenPanels()
        {

            return _messages.Count >0;
        }

        public bool GoToNextPanel()
        {
            var ret = _panelMessage.gameObject.activeSelf;
            OpenPanel();
            return ret;
        }
        public void Ready()
        {
            _onclosedPanels.OnNext(EUSystemType.UI);
        }
    }
   
}